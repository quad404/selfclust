package main

import (
	"bytes"
	"os"
	"path/filepath"
	"strings"
	"encoding/csv"

	log "github.com/golang/glog"
)

func nodesFunc() map[any]any {
	return map[any]any{
		"nodes": []int{1, 2, 3},
	}
}

func workdirFunc() map[any]any {
	cwd, err := os.Getwd()
	if err != nil {
		log.Errorf("Getwd(): %v", err)
	}
	return map[any]any{
		"workdir": filepath.Join(cwd, workdirPath),
	}
}

func ansibleFileFunc() map[any]any {
	data, err := os.ReadFile(ansibleFile)
	if err != nil {
		log.Errorf("ReadFile(): %v", err)
		return map[any]any{}
	}
	buf := bytes.NewBuffer(data)
	var nodes []map[string]string
	for {
		line, _ := buf.ReadString('\n')
		if len(line) < 2 {
			break
		}
		node := map[string]string{}
		pairs := inventoryRecords(line)
		if len(pairs) == 0 {
			continue
		}
		node["name"] = pairs[0]
		pairs = pairs[1:]
		for _, pair := range pairs {
			kv := strings.SplitN(pair, "=", 2)
			if len(kv) != 2 {
				continue
			}
			node[kv[0]] = kv[1]
		}
		nodes = append(nodes, node)
	}
	res := workdirFunc()
	res["nodes"] = nodes
	return res
}

func inventoryRecords(line string) []string {
	r := csv.NewReader(strings.NewReader(line))
	r.Comma = ' '
	r.LazyQuotes = true
	rec, err := r.Read()
	if err != nil {
		log.Warningf("CSV record read: %v", err)
		return nil
	}
	return rec
}
