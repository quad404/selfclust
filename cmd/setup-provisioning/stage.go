package main

import (
	"bytes"
	"fmt"
	"os"
	"path/filepath"
	"text/template"
)

type Stage struct {
	File        string
	ContextFunc func() map[any]any
}

func (s *Stage) Render() error {
	tmpl, err := template.ParseFiles(filepath.Join(tmplPath, s.File))
	if err != nil {
		return fmt.Errorf("parsing template: %v", err)
	}
	var buf bytes.Buffer
	if err := tmpl.Execute(&buf, s.ContextFunc()); err != nil {
		return fmt.Errorf("executing template: %v")
	}
	outFile := filepath.Join(workdirPath, s.File)
	if err := os.MkdirAll(filepath.Dir(outFile), 0750); err != nil {
		return fmt.Errorf("creating output dir: %v", err)
	}
	if err := os.WriteFile(outFile, buf.Bytes(), 0660); err != nil {
		return fmt.Errorf("writing output: %v", err)
	}
	return nil
}
