package main

import (
	"flag"

	log "github.com/golang/glog"
)

var (
	workdirPath = "_workdir"
	ansibleFile = "_workdir/virt-lightning/ansible.txt"
	tmplPath    = "templates/provisioning/"
	stages      = map[int][]Stage{
		1: {{"virt-lightning/virt-lightning.yaml", nodesFunc}},
		2: {
			{"salt/config/master", workdirFunc},
			{"salt/Saltfile", workdirFunc},
			{"salt/roster", ansibleFileFunc},
			{"salt/salt-ssh-copy-id.sh", ansibleFileFunc},
		},
	}

	stageID = flag.Int("stage", 1, "which stage to render (1, 2)")
)

func main() {
	flag.Parse()

	log.Infof("Setting up workdir [stage %d]...", *stageID)
	for _, stage := range stages[*stageID] {
		log.Infof(" - %s", stage.File)
		if err := stage.Render(); err != nil {
			log.Errorf("Rendering %s: %v", stage.File, err)
		}
	}
}
