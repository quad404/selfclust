#!/bin/sh

echo "Writing local VM configuration."
go run selfclust/cmd/setup-provisioning -logtostderr=true --stage=1

cd _workdir/virt-lightning/
echo "Starting local VMs."
virt-lightning up

echo "Writing ansible inventory file."
virt-lightning ansible_inventory > ansible.txt

cd $OLDPWD
echo "Writing Salt configuration."
go run selfclust/cmd/setup-provisioning -logtostderr=true --stage=2

cd _workdir/salt/
echo "Generating Salt SSH key. "
false | salt-ssh -i --no-host-keys '*' test.version >> /dev/null

echo "Deploying Salt SSH key and updating known_hosts."
sh ./salt-ssh-copy-id.sh

echo "Testing Salt SSH connection."
salt-ssh '*' test.version

cd $OLDPWD
