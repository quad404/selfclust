#!/bin/sh
{{ range .nodes }}
ssh-copy-id -i {{$.workdir}}/salt/tmp/pki/ssh/salt-ssh.rsa.pub -o GSSAPIAuthentication=no -o StrictHostKeyChecking=no {{.ansible_user}}@{{.ansible_host}}
{{ end }}
